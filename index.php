<?php
/*
 * Parsing position and work of Thai Judge
 * By K
 * Create : 2nd Aug, 2015
 */
include 'pdfparser/vendor/autoload.php';
include 'ordUTF8.php';

// Parse pdf file and build necessary objects.
$parser = new \Smalot\PdfParser\Parser();
$pdf = $parser->parseFile('judge.pdf');
$pages = [];

// Get Detail From All Pages of PDF
foreach($pdf->getPages() as $page):
    // 1.Clear LF and CRLF
    $cleanText = trim(preg_replace("/\r\n|\r|\n/","",$page->getText()));

    // 2.Split via ordered list
    $people = preg_split("/[๑๒๓๔๕๖๗๘๙๐]+\.|ทั้งนี้.*/",$cleanText,null,PREG_SPLIT_NO_EMPTY);
    array_shift($people); // Delete surplus
    
    // 3.Cleansing Data
    $newPeople = array_map(function($person){
        // Clear Excess White Space
        $person = preg_replace("/\s+/"," ",trim($person));
        
        // Extract New Position
        $extractArr = array_values(array_filter(preg_split("/\s+(ดํารงตําแหน่ง|ดํ ารงตําแหน่ง|ดํารงตําแหน่ ง)\s+/",$person)));
        
        // Extract Old Position
        $spacePos = mb_strrpos($extractArr[0]," ",-1);
        $oldPosition = trim(mb_strcut($extractArr[0], $spacePos));
        $spacePos = (preg_match("[๑|๒|๓|๔|๕|๖|๗|๘|๙]",$oldPosition))
                ? mb_strrpos($extractArr[0]," ",$spacePos - mb_strlen($extractArr[0]) - 1) : $spacePos;
        
        // Extract Title & Firstname
        if(mb_substr($extractArr[0],0,18) == 'นางสาว') $endTitlePos = 18;
        else $endTitlePos = 9;
        $endFirstNamePos = mb_strpos($extractArr[0]," ");
        
        // Return Array
        $returnArr = [
            "title" => mb_substr($extractArr[0],0,$endTitlePos),
            "firstName" => mb_substr($extractArr[0],$endTitlePos,$endFirstNamePos - $endTitlePos), 
            "lastName" => mb_substr($extractArr[0],$endFirstNamePos,$spacePos - $endFirstNamePos),
            "oldPosition" => mb_substr($extractArr[0],$spacePos),
            "newPosition" => $extractArr[1],
        ];
        return array_map(function($text){
            return trim($text);
        },$returnArr);
    }, $people);
    
    // 4.Correction
    foreach($newPeople as $key => $newPerson):
        if(preg_match("/ผู้พิพากษา/", $newPerson['lastName'])){
            $cutPos = mb_strpos($newPerson['lastName'],"ผู้พิพากษา");
            $newPerson['oldPosition'] = mb_strcut($newPerson['lastName'],$cutPos) . $newPerson['oldPosition'];
            $newPerson['lastName'] = trim(mb_strcut($newPerson['lastName'],0,$cutPos));
        }
        
        if(preg_match("/ภาค\s[๑|๒|๓|๔|๕|๖|๗|๘|๙]/",$newPerson['newPosition'])){
            if(substr_count($newPerson['newPosition'], " ") > 1){
                $newPerson['newPosition'] = preg_replace("/(\s)/", "", $newPerson['newPosition'],1);
            }
        } else $newPerson['newPosition'] = preg_replace("/(\s)/", "", $newPerson['newPosition']);
        
        $newPeople[$key] = $newPerson;
    endforeach;
    
    // 5. Convert each pages' data to JSON and Save in Array
    array_push($pages, $newPeople);
endforeach;

// Save all pages' data in JSON file.
$pages = json_encode($pages, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
$newFileName = getcwd() . '\judges.json';
$fp = fopen($newFileName, 'w');
fwrite($fp, $pages);
fclose($fp);

echo $newFileName . " is done!<br>";
var_dump(json_decode($pages,true));